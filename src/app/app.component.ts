import { Component } from '@angular/core';
import { BarsComponent } from './layouts/bars/bars.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { TopNavComponent } from './layouts/top-nav/top-nav.component';


@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent {
  title = 'app works!';
}
