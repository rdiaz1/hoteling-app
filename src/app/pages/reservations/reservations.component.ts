import { Component, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-reservations',
  templateUrl: 'reservations.component.html',
  styleUrls: ['reservations.component.css']
})
export class ReservationsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
