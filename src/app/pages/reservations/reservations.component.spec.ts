/* tslint:disable:no-unused-variable */

import { By }           from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { addProviders, async, inject } from '@angular/core/testing';
import { ReservationsComponent } from './reservations.component';

describe('Component: Reservations', () => {
  it('should create an instance', () => {
    let component = new ReservationsComponent();
    expect(component).toBeTruthy();
  });
});
