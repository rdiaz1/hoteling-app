import {Component, ViewEncapsulation, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import {CORE_DIRECTIVES} from '@angular/common';
import {DropdownModule} from 'ng2-bootstrap/ng2-bootstrap';

@Component({
    moduleId: module.id,
    selector: 'app-top-nav',
    templateUrl: 'top-nav.component.html',
    styleUrls: ['top-nav.component.css'],
    encapsulation: ViewEncapsulation.None,
    directives: [DropdownModule, CORE_DIRECTIVES, ROUTER_DIRECTIVES]
})
export class TopNavComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
