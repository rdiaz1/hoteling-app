import { HotelingFrontendPage } from './app.po';

describe('hoteling-frontend App', function() {
  let page: HotelingFrontendPage;

  beforeEach(() => {
    page = new HotelingFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
